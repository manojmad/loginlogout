import React, { Component } from 'react';
import 'antd/dist/antd.css';
import {Form,Input,Button} from 'antd';
import './labelView.css';


var index=0;
var employeedata=[];
export class SalaryTable extends Component {
    constructor() {
        super();
        this.state = {
            empName:'',
            designation:'',
            totalDays:'30',
            presentDays:'',
            leavesApproval:'2',
            leavesTaken:'',
            loseofPay:'',
            basicSalary:'',
            totalSalary:'',
            Savedata:[],
        };
      }
      handleName=(event)=> {
        this.setState({
          empName:event.target.value
         });
        }
        handleDesignation=(event)=> {
         
         this.setState({
          designation:event.target.value
          });
        }
       
         handlePresentdays=(event)=> {
          this.setState({
            presentDays:event.target.value
           });
           
         }
         handleBasicsalary=(event)=> {
          this.setState({
            basicSalary:event.target.value
           });
           
         }
    
      // handleChange = (event) => {
      //   this.setState({[event.target.name]: event.target.value});
      //   console.log("name",this.state.EmpName);
      // }
    
      handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err,values) => {
          if(!err){
          employeedata = [];
   let  leavesTaken = this.state.totalDays-this.state.presentDays;
   let salaryperday=this.state.basicSalary/30;
        employeedata=({
              key:index,
              empName: this.state.empName,
              designation:this.state.designation,
              totalDays:'30',
              presentDays: this.state.presentDays,
              leavesApproval:'2',
              leavesTaken:leavesTaken,
              loseofPay:leavesTaken>2 ?(leavesTaken-this.state.leavesApproval):0,
              basicSalary:this.state.basicSalary,
              totalSalary:leavesTaken>2 ?(this.state.totalDays-(leavesTaken-2))*salaryperday:this.state.basicSalary,
            });
            console.log('employeedata',employeedata.empName);
            console.log("copydata",leavesTaken);
                  this.setState({
                    Savedata: employeedata,
                  });
                  console.log('Savedata',employeedata);
                  this.props.form.resetFields();
                }
                  
              });
      }
    

    render(){
      const {getFieldDecorator} = this.props.form;
      const formItemLayout = {
       labelCol: {
         xs: { span: 14 },
         sm: { span: 15 },
       },
       wrapperCol: {
         xs: { span: 20 },
         sm: { span: 36 },
       },
     };
          
        return (
            <div>
              <Form>
                <div style={{ display:'Flex' }}>
                  <Form.Item
                  {...formItemLayout}
                  label='Employeename:'
                  hasFeedback
                  >
                    {getFieldDecorator('empName',{
                      initialValue: this.state.empName,
                      rules:[
                        {required:true, message:'Please input your employeename!'}
                      ]
                    })(
                  <Input type = 'text' style={{width: '100%'}} name="empName"  onChange={this.handleName} placeholder="Enter Your Name"/>
                    )
                    }
                    </Form.Item>
                    <Form.Item
                  {...formItemLayout}
                  label='designation:'
                  hasFeedback
                  >
                    {getFieldDecorator('designation',{
                      initialValue: this.state.designation,
                      rules:[
                        {required:true, message:'Please input your designation!'}
                      ]
                    })(
                  <Input type = 'text' style={{width: '100%'}} name="designation"  onChange={this.handleDesignation} placeholder="Enter Your designation"/>
                    )
                    }
                    </Form.Item>
                    <Form.Item
                  {...formItemLayout}
                  label='presentdays:'
                  hasFeedback
                  >
                    {getFieldDecorator('presentDays',{
                      initialValue: this.state.presentDays,
                      rules:[
                        {required:true, message:'Please input your presentdays!'}
                      ]
                    })(
                  <Input type = 'number' style={{width: '100%'}} name="presentDays"  onChange={this.handlePresentdays} placeholder="Enter Your presentdays"/>
                    )
                    }
                    </Form.Item>
                    <Form.Item
                  {...formItemLayout}
                  label='basicsalary:'
                  hasFeedback
                  >
                    {getFieldDecorator('basicSalary',{
                      initialValue: this.state.basicSalary,
                      rules:[
                        {required:true, message:'Please input your basicsalary!'}
                      ]
                    })(
                  <Input type = 'text' style={{width: '100%'}} name="basicSalary"  onChange={this.handleBasicsalary} placeholder="Enter Your basicsalary"/>
                    )
                    }
                    </Form.Item>
                </div>
                <Button type="default" style={{marginLeft:650}} onClick={this.handleSubmit}>Submit</Button> 
            </Form>
          <Form style={{border:'2 solid #000'}}
                       bordered >
                         <div style={{textAlign:'center'}}>
                <h1>VEDICBAHART TECHNOLOGY SOLUTIONS</h1>
                <h3>Employees Salary Details</h3>
                       </div>
                       <div className="border">
              <h1 className="header">Employee Details</h1>
   <label><h3><b className="labelclr">EmployeeName</b><b className="Namecol">:</b><i className="txt">{employeedata.empName}</i></h3></label><br></br>
   <label><h3><b className="labelclr">Designation</b><b className="Namecol">:</b><i className="txt">{employeedata.designation}</i></h3></label><br></br>
                        <h1 className="header">Salary Details</h1>
    <label><h3><b className="labelclr">TotalDays</b><b className="Namecol">:</b><i className="txt">{ employeedata.totalDays}</i></h3></label><br></br>
    <label><h3><b className="labelclr">presentDays</b><b className="Namecol">:</b><i className="txt">{ employeedata.presentDays}</i></h3></label><br></br>
    <label><h3><b className="labelclr">leavesApproval</b><b className="Namecol">:</b><i className="txt">{ employeedata.leavesApproval}</i></h3></label><br></br>
    <label><h3><b className="labelclr">LeavesTaken</b><b className="Namecol">:</b><i className="txt">{ employeedata.leavesTaken}</i></h3></label><br></br>
    <label><h3><b className="labelclr">loseofPay</b><b className="Namecol">:</b><i className="txt">{employeedata.loseofPay}</i></h3></label><br></br>
    <label><h3><b className="labelclr">BasicSalary</b><b className="Namecol">:</b><i className="txt">{ employeedata.basicSalary}</i></h3></label><br></br>
    <label><h3><b className="labelclr">TotalSalary</b><b className="Namecol">:</b><i className="txt">{ employeedata.totalSalary}</i></h3></label><br></br>
                      </div> 
                      </Form>
                    
            </div>
        )
    }
}

export default Form.create() (SalaryTable)
