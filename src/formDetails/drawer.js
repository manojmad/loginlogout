
import React ,{ Component } from 'react';
import { Drawer, Button, Icon, Table } from 'antd';
import 'antd/dist/antd.css';
import FormFields from './formFields';

class App extends Component {
  state = { visible: false };

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };
  getValue=(rowValues) => {
      this.setState({
          rowData: rowValues,
      });
  }

  render() {

    const columns = 
    [
        {
          title: 'Name',
          dataIndex: 'name',
          render: text=> <a href="www">{text}</a>,
        },
        {
            title: 'Salary',
            dataIndex: 'salary',
            
        },
        {
          title: 'Action',
          dataIndex: 'view',
           render:(record,index)=>{
           return(<Button onClick={() => this.toggle(record)}><Icon type="eye"/></Button>)
           }
          
        },
    ];

    return (
      <div
        style={{
          height: 480,
          overflow: 'hidden',
          position: 'relative',
          border: '1px solid #ebedf0',
          borderRadius: 2,
          padding: 48,
        //   textAlign: 'center',
          background: '#fafafa',
        }}
      >
        <div style={{ marginTop: 16 }}>

          <FormFields getValue={this.getValue}/> <br></br>
          <Button type="primary" onClick={this.showDrawer}>
            SalaryDetails
          </Button>
        </div>
        <Drawer
          title="Employee Salary Details"
          placement="right"
          closable={true}
          onClose={this.onClose}
          maskClosable={false}
          visible={this.state.visible}
          getContainer={false}
          style={{ position: 'absolute'}}
          width='600'
        >
         <Table
           columns={columns}
           dataSource={this.state.rowData}
         />
        </Drawer>
      </div>
    );
  }
}

export default App
          