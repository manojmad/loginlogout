import React, { Component } from 'react'
import {Form, Input, Icon , Button , message} from 'antd'
import 'antd/dist/antd.css';

let count = 0
export class formFields extends Component {
    constructor(){
        super();
            this.state={
                name: '',
                email: '',
                role: '',
                totalWorkingDays:'',
                tableArray: [],
              }
            }
  
  handleChange = (evt) => {
    this.setState({ [evt.target.name]: evt.target.value });
  }
  
  info = () => {
    message.success(`${this.state.name}'s Salary,added successfully`);
  };

  handleAdd=(index)=>{
    let   addDataArray = [...this.state.tableArray]
    let addArray={
          key:count,
          name: this.state.name,
          salary:(this.state.totalWorkingDays * 400)
      };
      addDataArray[count]=addArray;
      count++
      this.setState({
          tableArray:addDataArray, 
      },()=>{
          this.props.getValue(this.state.tableArray)
      });
    //   this.props.form.resetFields();
    this.info()
    
    }

  
  
handleReset = () => {
    this.setState({
        name: '',
        email: '',
        role: '',
        totalWorkingDays:'',
    })
    console.log("sdafh");
  }



    render() {

        return (
            <div>
                <Form>
                  <h3 style={{textAlign:'right'}}>TotalWorkingDays:25</h3>
                  <Input type="text" placeholder="Name" name="name" style={{width: 300}} onChange={this.handleChange} /> <br></br> <br></br>
                  <Input type="email" placeholder="Email" name="email" style={{width: 300}} onChange={this.handleChange} /> <br></br> <br></br>
                  <Input type="text" placeholder="Role" name="role" style={{width: 300}} onChange={this.handleChange} /> <br></br> <br></br>
                  <Input type="number" placeholder="TotalPresentDays" name="totalWorkingDays" style={{width: 300}} onChange={this.handleChange}/> <br></br> <br></br>
                  <Button onClick={this.handleAdd}><Icon type="plus"></Icon></Button> &nbsp;&nbsp;&nbsp;
                  <Button onClick={this.handleReset}>Clear</Button>
                </Form>
            </div>
        )
    }
}

export default formFields
