import React from "react";
// import ReactDOM from "react-dom";
import "antd/dist/antd.css";
import "./App.css";
import Logos from './images/titlelogo.png';
import { Form, Input, Button } from "antd";
// import Login from './login';

class RegistrationForm extends React.Component {
  state = {
    confirmDirty: false
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
      }
    });
  };

  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue("password")) {
      callback(" password that you enter is inconsistent!");
    } else {
      callback();
    }
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0
        },
        sm: {
          span: 16,
          offset: 8
        }
      }
    };

    return (


      
      
      <div className="bord">
        <Form {...formItemLayout} onSubmit={this.handleSubmit}>
          <div className="logos">
                <img src={Logos} alt="name"></img>
            </div>
            <Form layout="vertical">
            <Form.Item>
            <p className="sign">
              Please sign in to your account
            </p>
          </Form.Item>
          <Form.Item label="E-mail / Phone Number" className="Yassword">
            {getFieldDecorator("email", {
              rules: [
                {
                  type: "email",
                  message: "The input is not valid E-mail!"
                },
                {
                  required: true,
                  message: "Please input your E-mail!"
                }
              ]
            })(<Input style={{ width: "62%" , backgroundColor: '#08D5E0', border: '1px solid #27aeb5'
         }} />)}
          </Form.Item>
          <Form.Item label="Password" className="Wassword"       hasFeedback>
            {getFieldDecorator("password", {
              rules: [
                {
                  required: true,
                  message: "Please input your password!"
                },
                {
                  validator: this.validateToPassword
                }
              ]
            })(<Input.Password style={{width:'62%' , backgroundColor: '#08D5E0'}}  />)}
          </Form.Item>

          <Form.Item {...tailFormItemLayout}>
            <Button className="give" type="primary" htmlType="submit">
              Login
            </Button>
          </Form.Item>
          
          <Form.Item>
            <p className="register">No account ? Register now</p>
          </Form.Item>
          </Form>
        </Form>
      </div>
      
    );
  }
}

const WrappedForm = Form.create({ name: "register" })(
  RegistrationForm
);
export default  WrappedForm;
