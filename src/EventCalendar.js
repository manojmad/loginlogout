import React, { Component } from 'react';
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import './main.scss';


export class EventCalendar extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }
    
    render() {
        return (
            <div>
               <FullCalendar 
               defaultView="dayGridWeek" 
               plugins={[ dayGridPlugin ]} 
               /> 
               
            </div>
        )
    }
}

export default EventCalendar;
