import React from "react";
// import ReactDOM from "react-dom";
import "antd/dist/antd.css";
import "./App.css";
// import Bell from  '../src/images/page.png';
import Logo from '../src/images/titlelogo.png';
import { Form, Input, Button } from "antd";

class RegistrationForm extends React.Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: []
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
      }
    });
  };

  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue("password")) {
      callback("Two passwords that you enter is inconsistent!");
    } else {
      callback();
    }
  };

 
  

  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0
        },
        sm: {
          span: 16,
          offset: 8
        }
      }
    };

    return (
      // <div>
      //   <p>hghf</p>
      // </div>

        <div className="border">
      <Form {...formItemLayout} onSubmit={this.handleSubmit}>
      <div className="logos">
                <img src={Logo} alt="name"></img>
            </div>
            <Form layout="vertical">
        <Form.Item label="Name">
          {getFieldDecorator("Name", {
            rules: [
              {
                type: "Name",
                message: "The input is not valid name"
              },
              {
                required: true,
                message: "Please input your name"
              }
            ]
          })(<Input style={{ width: "90%" }} />)}
        </Form.Item>

        <Form.Item label="E-Mail">
          {getFieldDecorator("E-Mail", {
            rules: [
              {
                type: "E-Mail",
                message: "The input is not valid E-mail"
              },
              {
                required: true,
                message: "Please input your E-mail"
              }
            ]
          })(<Input style={{ width: "90%" }} />)}
        </Form.Item>

        <Form.Item label="Phone Number">
          {getFieldDecorator("phone", {
            rules: [
              { required: true, message: "Please input your phone number!" }
            ]
          })(<Input style={{ width: "90%" }} />)}
        </Form.Item>

        <Form.Item label="Password" hasFeedback>
          {getFieldDecorator("password", {
            rules: [
              {
                required: true,
                message: "Please input your password!"
              },
              {
                validator: this.validateToNextPassword
              }
            ]
          })(<Input.Password style={{ width: "90%" }} />)}
        </Form.Item>

        <Form.Item {...tailFormItemLayout}>
          {getFieldDecorator("agreement", {
            valuePropName: "checked"
          })(<p>I have agreed to terms and conditions</p>)}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit">
            Register
          </Button>
        </Form.Item>

        <Form.Item {...tailFormItemLayout}>
          {getFieldDecorator("agreement", {
            valuePropName: "checked"
          })(<p>Already have an account ? Sign in</p>)}
        </Form.Item>



           
      </Form>
      </Form>
      </div>
    );
  }
}

const WrappedRegistrationForm = Form.create({ name: "register" })(
  RegistrationForm
);

// ReactDOM.render(
//   <WrappedRegistrationForm />,
//   document.getElementById("container")
// );


export default  WrappedRegistrationForm;