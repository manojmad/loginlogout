import React, {Component} from 'react';
import 'antd/dist/antd.css';
import { Table, Button, Icon,message, Input } from 'antd';
// import {tableRow} from './rowData';
// import Notification from './notification';

// rowSelection object indicates the need for row selection
// let obj = [];
// let rowData;
// let tableArray;

// let isEnabled;
// const rowSelection = {
//   onChange: (selectedRowKeys, selectedRows) => {
//     rowData = selectedRows;
//     obj = rowData ; 
//     console.log(`selectedRowKeys: ${selectedRowKeys}`,obj);
//   }
// }; 

class data extends Component {
  constructor(){
    super();
     this.state={
        value:[],
        // salary: '',
        employeValues:[
          {
          id:'1',
          name:'WebDeveloper',
          workedExperience:''
        },
        {
          id:'2',
          name:'UI Designer',
          workedExperience:''
        },
        {
          id:'3',
          name:'Network Engineer',
          workedExperience:''
        },
        {
          id:'4',
          name:'Admin',
          workedExperience:''
        },
        {
          id:'5',
          name:'Database Management',
          workedExperience:''
        },
        {
          id:'6',
          name:'software tester',
          workedExperience:''
        },

      ]
     }
  }
  info = (record) => {
    message.success(`${record.name} \u00A0 Added Successfully`);
  };
  handleChange=(value,record)=>{
    record.workedExperience = value;
 }  
 handleAdd = (record,index) => {
  const filterTechnology= this.state.value.filter(tech=>tech.name===record.name)
  if(filterTechnology.length===0){
   this.setState({
      value: [...this.state.value,record],
    },()=>{
     this.props.setSelectedListValue(this.state.value)
 });
 this.info(record)
  }else{

  }
  console.log("record",record);
}

render(){
 
  const columnData = 
  [
      {
        title: 'name',
        dataIndex: 'name',
        key: 'name',
        render: text => text
      },
      {
        title: 'Worked Experience',
        dataIndex: 'workedExperience',
        render:(text,record,index) => (
          <Input type="number" name="workedExperience" placeholder="Amount" onChange={(event) => this.handleChange(event.target.value,record)}/>)
      },
      {
        title: 'Action',
        dataIndex: '',
        key: 'x',
        render:(text,record,index) => {
          return( <Button  onClick={() => this.handleAdd(record,index)}><Icon type="plus"/></Button>)
        } 
      }
    ]
    return(
   <div>   
   <h2>Technologies</h2>
      <Table
        bordered
        columns={columnData}
        dataSource={this.state.employeValues}
        rowKey={(record,index)=>index+1}
       />    
   </div>
    )
 }
}

 export default data;
          